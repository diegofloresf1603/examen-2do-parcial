﻿using System;
using System.Collections.Generic;
using System.Text;
//El patrón Observer puede ser utilizado cuando hay objetos que dependen de otro, 
//necesitando ser notificados en caso de que se produzca algún cambio en él.
namespace examen2do
{
    interface IOBSERVER
    {
        // utilizamos el metodo update en modelo de push ya que informa a sus 
        // suscriptores con sus valores cuando estos cambien de estado 
        void Update(string mensaje);
        

    }
}
