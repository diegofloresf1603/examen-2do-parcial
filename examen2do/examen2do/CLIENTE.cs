﻿using System;
using System.Collections.Generic;
using System.Text;
//El patrón Observer puede ser utilizado cuando hay objetos que dependen de otro, 
//necesitando ser notificados en caso de que se produzca algún cambio en él.
namespace examen2do
{
    class CLIENTE
    {
        // lista de observadores en donde se guardaran todos aquellos que estan suscriptos al mes de netflix
        private List<IOBSERVER> observadores = new List<IOBSERVER>();
        private string mensaje;
        private Random rnd = new Random();
        private int i;

        public int I { get => i; set => i = value; }
        

        // metodo de suscripcion a la lista
        public void Suscribir (IOBSERVER suscrito)
        {
            // para agregar a la lista 
            observadores.Add(suscrito); 

        }

        // metodo de cancelar suscripcion
        public void Cancelarsuscripcion (IOBSERVER suscrito)
        {
            // remover al cliente que cancelo la suscripcion de la lista
            observadores.Remove(suscrito);
        }
        


        // metodo de notificar que nos sirve para indicar a todos los observadoresque estan suscriptos 
        // y que ha ocurrido un cambio en el estado
        public void Notificar()
        {
            foreach(IOBSERVER o in observadores)
            {
                o.Update(mensaje);
            }
        }
        public void Proceso()
        {
            i = rnd.Next(12);
            if (i%2==0)
            {
                Console.ForegroundColor = ConsoleColor.Blue ;
                Console.WriteLine("----Suscripciones----");
                mensaje = string.Format("suscripto a mes premium Netflix " + "", i);
                Notificar();

            }    
        }

    }
}
