﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//El patrón Observer puede ser utilizado cuando hay objetos que dependen de otro, 
//necesitando ser notificados en caso de que se produzca algún cambio en él.
namespace examen2do
{
    class OBSERVADOR: IOBSERVER
    {
        // se crean variables para agregar observadores
        private string nombre;
        private CLIENTE cliente;


        
        public OBSERVADOR (string pNombre, CLIENTE pCliente)
        {
            nombre = pNombre;
            cliente = pCliente;
            cliente.Suscribir(this);

        }


        // este metodo lo utilizamos por el modelo push
        public void Update(string mensaje)
        {
            // update con push
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" {0}-{1}", nombre, mensaje);
        }
       
    }
}
