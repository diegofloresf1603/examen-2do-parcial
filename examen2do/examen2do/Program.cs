﻿using System;
//El patrón Observer puede ser utilizado cuando hay objetos que dependen de otro, 
//necesitando ser notificados en caso de que se produzca algún cambio en él.
namespace examen2do
{
    class Program
    {

        static void Main(string[] args)

        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("-----este programa verifica si un cliente sigue o no suscripto a un mes de netlfix -----");
            // creamos el cliente 
            CLIENTE miCliente = new CLIENTE();
            // creamos los observadores 
                OBSERVADOR cliente1 = new OBSERVADOR("Maria Zambrano", miCliente);
                OBSERVADOR cliente2 = new OBSERVADOR("Jose Rodriguez", miCliente);
                OBSERVADOR cliente3 = new OBSERVADOR("Alberto Plaza", miCliente);
                OBSERVADOR cliente4 = new OBSERVADOR("Isabella Bernardi", miCliente);

            // se realiza el proceso para simular el cambio de estado 
            for (int n = 0; n < 5; n++)
                    miCliente.Proceso();


            // ejemplo de cambio de estado con la cancelacion de un observador 
            // en esta caso se cancelo la suscripcion del mes de netflix al cliente2
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("-----cancelar suscripcion Jose Rodriguez -----");

            miCliente.Cancelarsuscripcion(cliente2);
            for (int n = 0; n < 5; n++)
                miCliente.Proceso();


            // otro ejemplo de cambio de estado  y con la cancelacion de un observador 
            // en esta caso se cancelo la suscripcion del mes de netflix al cliente1
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("-----cancelar suscripcion Maria Zambrano -----");

            miCliente.Cancelarsuscripcion(cliente1);
            for (int n = 0; n < 5; n++)
                miCliente.Proceso();

        }
    }
}
